import cluster from 'node:cluster';
import { WebApplication } from './WebApplication';

export class ClusteredWebApplication extends WebApplication {

  public async run() {
    await this.init();

    if (cluster.isPrimary) {
      const workersCount = this.config.workers;
      this.logger.info(`Starting ${workersCount} workers`);
      for (let i = 0; i < workersCount; i += 1) {
        cluster.fork();
      }
    } else {
      super.run();
    }
  }
}
