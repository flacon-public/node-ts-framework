import { resolve } from 'path';
import { Container } from 'inversify';

export abstract class Module {

  public get controllers() {
    return resolve(this.baseDirectory, 'application/controllers/*.js');
  }

  public get migrations() {
    return resolve(this.baseDirectory, 'infrastructure/migrations/*.js');
  }

  public get models() {
    return resolve(this.baseDirectory, 'infrastructure/models/*.js');
  }

  protected abstract get baseDirectory(): string;

  public abstract initDiContainer(
    container: Container,
    allApplicationModules: Module[],
  ): Promise<void>;
  public abstract end(container: Container): Promise<void>;

}
