import { injectable } from 'inversify';
// tslint:disable-next-line: import-name
import Redis from 'ioredis';
import { RedisConfig } from '@melmedia/config';

import { inject, Type } from '../di';

@injectable()
export class RedisConnectionFactory {
  @inject(Type.RedisConfig)
  protected config!: RedisConfig;

  public async create() {
    const connection = new Redis(this.config);
    await new Promise<void>((resolve, reject) => {
      connection.on('connect', () => {
        resolve();
      });
      connection.on('error', (err) => {
        connection.quit();
        reject(err);
      });
    });
    return connection;
  }
}
