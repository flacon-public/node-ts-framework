import { injectable } from 'inversify';
import { DataSource } from 'typeorm';
import { DbConfig } from '@melmedia/config';

import { inject, Type } from '../di';
import { Module } from '../Module';
import { TypeormLogger } from '../log/TypeormLogger';

/**
 * TODO timezone
 */
@injectable()
export class DbConnectionFactory {
  @inject(Type.DbConfig)
  protected dbConfig!: DbConfig;

  public create(modules: Module[]): DataSource {
    return new DataSource({
      logger: new TypeormLogger,
      ...this.getConfig(modules),
    });
  }

  public getConfig(modules: Module[]) {
    return {
      ...this.dbConfig,
      migrations: modules.map(module => module.migrations),
      entities: modules.map(module => module.models),
    };
  }

}
