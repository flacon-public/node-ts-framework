"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
exports.Module = void 0;
const path_1 = require("path");
class Module {
    get controllers() {
        return (0, path_1.resolve)(this.baseDirectory, 'application/controllers/*.js');
    }
    get migrations() {
        return (0, path_1.resolve)(this.baseDirectory, 'infrastructure/migrations/*.js');
    }
    get models() {
        return (0, path_1.resolve)(this.baseDirectory, 'infrastructure/models/*.js');
    }
}
exports.Module = Module;
//# sourceMappingURL=Module.js.map