"use strict";
var __createBinding = (this && this.__createBinding) || (Object.create ? (function(o, m, k, k2) {
    if (k2 === undefined) k2 = k;
    var desc = Object.getOwnPropertyDescriptor(m, k);
    if (!desc || ("get" in desc ? !m.__esModule : desc.writable || desc.configurable)) {
      desc = { enumerable: true, get: function() { return m[k]; } };
    }
    Object.defineProperty(o, k2, desc);
}) : (function(o, m, k, k2) {
    if (k2 === undefined) k2 = k;
    o[k2] = m[k];
}));
var __setModuleDefault = (this && this.__setModuleDefault) || (Object.create ? (function(o, v) {
    Object.defineProperty(o, "default", { enumerable: true, value: v });
}) : function(o, v) {
    o["default"] = v;
});
var __importStar = (this && this.__importStar) || function (mod) {
    if (mod && mod.__esModule) return mod;
    var result = {};
    if (mod != null) for (var k in mod) if (k !== "default" && Object.prototype.hasOwnProperty.call(mod, k)) __createBinding(result, mod, k);
    __setModuleDefault(result, mod);
    return result;
};
Object.defineProperty(exports, "__esModule", { value: true });
exports.PaginationByOffset = void 0;
const url = __importStar(require("url"));
const querystring = __importStar(require("querystring"));
class PaginationByOffset {
    async get(limit, offset, callback, request, response) {
        if (undefined === limit || 0 === limit) {
            return callback(undefined);
        }
        let data = await callback(limit + 1);
        const isHaveNextPage = data && data.length > limit;
        data = data.slice(0, limit);
        if (isHaveNextPage) {
            const nextPageUrl = url.parse(request.url, true);
            // tslint:disable-next-line:prefer-template
            nextPageUrl.search = '?' + querystring.stringify(Object.assign(Object.assign({}, nextPageUrl.query), { offset: (offset || 0) + limit }));
            response.links({ next: url.format(nextPageUrl) });
        }
        return data;
    }
}
exports.PaginationByOffset = PaginationByOffset;
//# sourceMappingURL=PaginationByOffset.js.map