"use strict";
var __createBinding = (this && this.__createBinding) || (Object.create ? (function(o, m, k, k2) {
    if (k2 === undefined) k2 = k;
    var desc = Object.getOwnPropertyDescriptor(m, k);
    if (!desc || ("get" in desc ? !m.__esModule : desc.writable || desc.configurable)) {
      desc = { enumerable: true, get: function() { return m[k]; } };
    }
    Object.defineProperty(o, k2, desc);
}) : (function(o, m, k, k2) {
    if (k2 === undefined) k2 = k;
    o[k2] = m[k];
}));
var __setModuleDefault = (this && this.__setModuleDefault) || (Object.create ? (function(o, v) {
    Object.defineProperty(o, "default", { enumerable: true, value: v });
}) : function(o, v) {
    o["default"] = v;
});
var __importStar = (this && this.__importStar) || function (mod) {
    if (mod && mod.__esModule) return mod;
    var result = {};
    if (mod != null) for (var k in mod) if (k !== "default" && Object.prototype.hasOwnProperty.call(mod, k)) __createBinding(result, mod, k);
    __setModuleDefault(result, mod);
    return result;
};
Object.defineProperty(exports, "__esModule", { value: true });
exports.PaginationByAttribute = void 0;
const url = __importStar(require("url"));
const querystring = __importStar(require("querystring"));
const util = __importStar(require("util"));
class PaginationByAttribute {
    constructor(orderingAttributeName) {
        this.orderingAttributeName = orderingAttributeName;
    }
    async get(limit, callback, request, response) {
        if (undefined === limit) {
            return callback(undefined);
        }
        let data = await callback(limit + 1);
        const isHaveNextPage = data && data.length > limit;
        data = data.slice(0, limit);
        if (isHaveNextPage) {
            const nextOrderingAttibuteValue = data[data.length - 1][this.orderingAttributeName];
            /* tslint:disable-next-line:max-line-length */
            const orderingParamName = `after${this.orderingAttributeName[0].toUpperCase()}${this.orderingAttributeName.slice(1)}`;
            const nextPageUrl = url.parse(request.url, true);
            // tslint:disable-next-line:prefer-template
            nextPageUrl.search = '?' + querystring.stringify(Object.assign(Object.assign({}, nextPageUrl.query), { [orderingParamName]: util.format(nextOrderingAttibuteValue) }));
            response.links({ next: url.format(nextPageUrl) });
        }
        return data;
    }
}
exports.PaginationByAttribute = PaginationByAttribute;
//# sourceMappingURL=PaginationByAttribute.js.map