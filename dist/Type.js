"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
exports.Type = void 0;
exports.Type = {
    ServerConfig: Symbol('ServerConfig'),
    LogConfig: Symbol('LogConfig'),
    DbConfig: Symbol('DbConfig'),
    DbConnection: Symbol('DbConnection'),
    RedisConfig: Symbol('RedisConfig'),
    RedisConnection: Symbol('RedisConnection'),
    ServicesConfig: Symbol('ServicesConfig'),
    ServiceDiscovery: Symbol('ServiceDiscovery'),
    AppLogger: Symbol('AppLogger'),
    AccessLogger: Symbol('AccessLogger'),
    DbLogger: Symbol('DbLogger'),
};
//# sourceMappingURL=Type.js.map