"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
exports.Environment = void 0;
var Environment;
(function (Environment) {
    Environment["Development"] = "dev";
    Environment["QA"] = "qa";
    Environment["Production"] = "prod";
})(Environment = exports.Environment || (exports.Environment = {}));
//# sourceMappingURL=Environment.js.map