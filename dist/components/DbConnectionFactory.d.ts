import { DataSource } from 'typeorm';
import { DbConfig } from '@melmedia/config';
import { Module } from '../Module';
/**
 * TODO timezone
 */
export declare class DbConnectionFactory {
    protected dbConfig: DbConfig;
    create(modules: Module[]): DataSource;
    getConfig(modules: Module[]): {
        migrations: string[];
        entities: string[];
        type: "postgres";
        host: string;
        database: string;
        username: string;
        password: string;
    };
}
