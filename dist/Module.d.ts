import { Container } from 'inversify';
export declare abstract class Module {
    get controllers(): string;
    get migrations(): string;
    get models(): string;
    protected abstract get baseDirectory(): string;
    abstract initDiContainer(container: Container, allApplicationModules: Module[]): Promise<void>;
    abstract end(container: Container): Promise<void>;
}
