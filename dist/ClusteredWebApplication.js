"use strict";
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
exports.ClusteredWebApplication = void 0;
const node_cluster_1 = __importDefault(require("node:cluster"));
const WebApplication_1 = require("./WebApplication");
class ClusteredWebApplication extends WebApplication_1.WebApplication {
    async run() {
        await this.init();
        if (node_cluster_1.default.isPrimary) {
            const workersCount = this.config.workers;
            this.logger.info(`Starting ${workersCount} workers`);
            for (let i = 0; i < workersCount; i += 1) {
                node_cluster_1.default.fork();
            }
        }
        else {
            super.run();
        }
    }
}
exports.ClusteredWebApplication = ClusteredWebApplication;
//# sourceMappingURL=ClusteredWebApplication.js.map