"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
exports.rejectNanParam = void 0;
const http_errors_1 = require("@melmedia/http-errors");
function rejectNanParam(param, value) {
    if (isNaN(value)) {
        throw new http_errors_1.BadRequestError(`${param} must be a number`);
    }
}
exports.rejectNanParam = rejectNanParam;
//# sourceMappingURL=rejectNanParam.js.map