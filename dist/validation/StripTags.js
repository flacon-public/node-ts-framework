"use strict";
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
exports.StripTags = void 0;
const class_transformer_1 = require("class-transformer");
const striptags_1 = __importDefault(require("striptags"));
/* tslint:disable-next-line:function-name */
function StripTags(options, stripTagsOptions) {
    return (0, class_transformer_1.Transform)(({ value }) => {
        if (stripTagsOptions && stripTagsOptions.each) {
            if (Array.isArray(value)) {
                return value.map(stripTagsWrapper);
            }
        }
        else {
            return stripTagsWrapper(value);
        }
    }, options);
}
exports.StripTags = StripTags;
function stripTagsWrapper(value) {
    return undefined !== value && null !== value ? (0, striptags_1.default)(value) : value;
}
//# sourceMappingURL=StripTags.js.map