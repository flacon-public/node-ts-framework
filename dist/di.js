"use strict";
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
exports.Type = exports.inject = exports.container = void 0;
const inversify_1 = require("inversify");
const inversify_inject_decorators_1 = __importDefault(require("inversify-inject-decorators"));
const Type_1 = require("./Type");
Object.defineProperty(exports, "Type", { enumerable: true, get: function () { return Type_1.Type; } });
exports.container = new inversify_1.Container({ defaultScope: 'Singleton' });
exports.inject = (0, inversify_inject_decorators_1.default)(exports.container).lazyInject;
//# sourceMappingURL=di.js.map